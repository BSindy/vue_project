import Mock from 'mockjs'

const data = Mock.mock({
  'items|30': [{
    'id|+1': 1,
    url: Mock.Random.url('http'),
    nodelist: '@sentence(10, 20)',
    'webdriver|1': ['firefox', 'chrome', 'ie']
  }]
})

export default [
  {
    url: '/vue-admin-template/server/list',
    type: 'get',
    response: config => {
      const { id, page = 1, limit = 10 } = config.query
      const items = data.items
      const mockList = items.filter(item => {
        if (id && item.id.indexOf(id) < 0) return false
        return true
      })
      const pageList = mockList.filter((item, index) => index < limit * page && index >= limit * (page - 1))
      return {
        code: 20000,
        data: {
          total: mockList.length,
          items: pageList
        }
      }
    }
  }
]
