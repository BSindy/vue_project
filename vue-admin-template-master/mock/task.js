import Mock from 'mockjs'

const data = Mock.mock({
  1: {
    'items|30': [{
      'id|+1': 1,
      'name|1': '@string',
      date: '@datetime',
      'num|1-100': 1,
      'status|1': ['完成', '失败', '中断'],
      'issues|1': '@cparagraph',
      'version|1-9.1': 1,
      'task_num|1-100': 1
    }]
  },
  2: {
    'items|30': [{
      'id|+1': 1,
      'name|1': '@string',
      date: '@datetime',
      'num|1-100': 1,
      'status|1': ['完成', '失败', '中断'],
      'issues|1': '@cparagraph',
      'version|1-9.1': 1,
      'task_num|1-100': 1
    }]
  }
})

export default [
  // task info
  {
    url: '/vue-admin-template/project/task',
    type: 'get',
    response: config => {
      const { id } = config.query
      const info = data[id]
      if (!info) {
        return {
          code: 50008,
          message: '获取任务信息失败'
        }
      }
      return {
        code: 20000,
        data: info
      }
    }
  }
]
