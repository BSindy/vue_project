import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/register',
    component: () => import('@/views/register/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '概览', icon: 'dashboard' }
    }]
  },

  {
    path: '/project',
    component: Layout,
    redirect: '/project/list',
    name: 'Example',
    meta: { title: '项目', icon: 'example' },
    children: [
      {
        path: 'list',
        name: 'List',
        component: () => import('@/views/project/list/index'),
        meta: { title: '项目列表', icon: 'table' }
      },
      {
        path: 'newPro',
        name: 'NewPro',
        component: () => import('@/views/project/newPro/index'),
        meta: { title: '新建项目', icon: 'tree' }
      },
      {
        path: 'taskList/:id(\\d+)',
        name: 'Task',
        component: () => import('@/views/task/list/index'),
        meta: { title: '任务列表', icon: 'tree' },
        hidden: true
      }
    ]
  },

  {
    path: '/task',
    component: Layout,
    redirect: '/task/list',
    name: 'Task',
    meta: { title: '测试任务', icon: 'example' },
    children: [
      {
        path: 'list',
        name: 'List',
        component: () => import('@/views/task/list/index'),
        meta: { title: '任务列表', icon: 'table' }
      },
      {
        path: 'taskExe',
        name: '任务执行',
        component: () => import('@/views/task/taskExe/index'),
        meta: { title: '执行任务', icon: 'tree' }
      }
    ],
    hidden: true
  },

  {
    path: '/version',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Version',
        component: () => import('@/views/version/index'),
        meta: { title: '版本', icon: 'form' }
      }
    ]
  },

  {
    path: '/server',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/server/index'),
        meta: { title: '服务器', icon: 'form' }
      }
    ]
  },

  {
    path: '/manage',
    component: Layout,
    redirect: '/manage/qdmanage',
    name: 'Manage',
    meta: {
      title: '质量数据',
      icon: 'nested'
    },
    children: [
      {
        path: 'qdmanage',
        component: () => import('@/views/manage/qdmanage/index'), // Parent router-view
        name: 'Menu1',
        meta: { title: '质量数据管理' }
      },
      {
        path: 'qdconf',
        component: () => import('@/views/manage/qdconf/index'),
        meta: { title: '质量数据配置项' }
      }
    ]
  },

  {
    path: '/analysis',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'analysis',
        component: () => import('@/views/analysis/index'),
        meta: { title: '测试问题分析', icon: 'link' }
      }
    ]
  },

  {
    path: '/report',
    component: Layout,
    redirect: '/report/list',
    name: 'Report',
    meta: { title: '质量报告', icon: 'example' },
    children: [
      {
        path: 'conf',
        name: 'Conf',
        component: () => import('@/views/report/conf/index'),
        meta: { title: '质量报告配置', icon: 'table' }
      },
      {
        path: 'list',
        name: 'Tree',
        component: () => import('@/views/report/list/index'),
        meta: { title: '质量报告查看', icon: 'tree' }
      },
      {
        path: 'subitem',
        name: 'Subitem',
        component: () => import('@/views/report/view/subitem'),
        meta: { title: '质量报告-分项', icon: 'tree' },
        hidden: true
      },
      {
        path: 'comprehensive',
        name: 'Comprehensive',
        component: () => import('@/views/report/view/comprehensive'),
        meta: { title: '质量报告-综合', icon: 'tree' },
        hidden: true
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
