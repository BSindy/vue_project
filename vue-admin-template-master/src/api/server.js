import request from '@/utils/request'

export function getServerList(params) {
  return request({
    url: '/vue-admin-template/server/list',
    method: 'get',
    params
  })
}
