import request from '@/utils/request'

export function getTaskInfo(params) {
  return request({
    url: '/vue-admin-template/project/task',
    method: 'get',
    params
  })
}
