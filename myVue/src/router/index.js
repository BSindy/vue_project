import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/pages/1_Login'
import NewUser from '@/components/pages/2_NewUser'
import NewProject from '@/components/pages/3_NewProject'
import NewProVersion from '@/components/pages/4_NewProVersion'
import TestServer from '@/components/pages/5_TestServer'
import ProjectList from '@/components/pages/6_ProjectList'
import ProNameVer from '@/components/pages/7_ProNameVer'
import TaskExecution from '@/components/pages/8_TaskExecution'
import NewTask from '@/components/pages/9_NewTask'
import PerformTest from '@/components/pages/10_PerformTest'
import ViewTask from '@/components/pages/11_ViewTask'
import MonitorInfo from '@/components/pages/12_MonitorInfo'
import ProblemAnalysis from '@/components/pages/13_ProblemAnalysis'
import QualityManage from '@/components/pages/14_QualityManage'
import QualityDataConf from '@/components/pages/15_QualityDataConf'
import QualityRepConf from '@/components/pages/16_QualityRepConf'
import QualityRepCompre from '@/components/pages/17_QualityRepCompre'
import QualityRepItem from '@/components/pages/18_QualityRepItem'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/1_Login',
      component: Login
    },{
      path: '/2_NewUser',
      component: NewUser
    },{
      path: '/3_NewProject',
      component: NewProject
    },{
      path: '/4_NewProVersion',
      component: NewProVersion
    },{
      path: '/5_TestServer',
      component: TestServer
    },{
      path: '/6_ProjectList',
      component: ProjectList
    },{
      path: '/7_ProNameVer',
      component: ProNameVer
    },{
      path: '/8_TaskExecution',
      component: TaskExecution
    },{
      path: '/9_NewTask',
      component: NewTask
    },{
      path: '/10_PerformTest',
      component: PerformTest
    },{
      path: '/11_ViewTask',
      component: ViewTask
    },{
      path: '/12_MonitorInfo',
      component: MonitorInfo
    },{
      path: '/13_ProblemAnalysis',
      component: ProblemAnalysis
    },{
      path: '/14_QualityManage',
      component: QualityManage
    },{
      path: '/15_QualityDataConf',
      component: QualityDataConf
    },{
      path: '/16_QualityRepConf',
      component: QualityRepConf
    },{
      path: '/17_QualityRepCompre',
      component: QualityRepCompre
    },{
      path: '/18_QualityRepItem',
      component: QualityRepItem
    }
  ]
})
